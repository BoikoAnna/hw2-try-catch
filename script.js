//Теоретичне питання
//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

//Синтаксична конструкція try...catch, дозволяє нам “перехоплювати” помилки, що дає змогу
//скриптам виконати потрібні дії, а не раптово припинити роботу
// таким чином при помилці try скрипт не падає, а ми отримуємо можливість опрацювати помилку всередині catch

// Наприклад з backend повинен прийти масив, а прийшов об'єкт 
// ми починаємо працювати як з масивом, а код не працює, тоді варто обернути його в try...catch




// Завдання
// Дано масив books.

// const books = [
//   { 
//     author: "Люсі Фолі",
//     name: "Список запрошених",
//     price: 70 
//   }, 
//   {
//    author: "Сюзанна Кларк",
//    name: "Джонатан Стрейндж і м-р Норрелл",
//   }, 
//   { 
//     name: "Дизайн. Книга для недизайнерів.",
//     price: 70
//   }, 
//   { 
//     author: "Алан Мур",
//     name: "Неономікон",
//     price: 70
//   }, 
//   {
//    author: "Террі Пратчетт",
//    name: "Рухомі картинки",
//    price: 40
//   },
//   {
//    author: "Анґус Гайленд",
//    name: "Коти в мистецтві",
//   }
// ];

// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список 
//(схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність 
// (в об'єкті повинні міститися всі три властивості - author, name, price). 
// Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає
//  в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
// Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React




// const books = [
//   { 
//     author: "Люсі Фолі",
//     name: "Список запрошених",
//     price: 70 
//   }, 
//   {
//    author: "Сюзанна Кларк",
//    name: "Джонатан Стрейндж і м-р Норрелл",
//   }, 
//   { 
//     name: "Дизайн. Книга для недизайнерів.",
//     price: 70
//   }, 
//   { 
//     author: "Алан Мур",
//     name: "Неономікон",
//     price: 70
//   }, 
//   {
//    author: "Террі Пратчетт",
//    name: "Рухомі картинки",
//    price: 40
//   },
//   {
//    author: "Анґус Гайленд",
//    name: "Коти в мистецтві",
//   }
// ];
  
//   const root = document.querySelector("#root");
//   const booksList = document.createElement("ul");
  
//   books.forEach((book, i) => {
//     try {
//       if (!book.hasOwnProperty("author")) {
//         throw new Error(`The Autor is absent in ${i + 1} books`);
//       }
//       if (!book.hasOwnProperty("name")) {
//         throw new Error(`The Name is absent in  ${i + 1} books`);
//       }
//       if (!book.hasOwnProperty("price")) {
//         throw new Error(`The price is absent in  ${i + 1} books`);
//       }
  
//       const { author, name, price } = book;
  
//       const li = document.createElement("li");
//       const authorParagraph = document.createElement("p");
//       const nameParagraph = document.createElement("p");
//       const priceParagraph = document.createElement("p");
  
//       authorParagraph.textContent = `autor: ${author}`;
//       nameParagraph.textContent = `name: ${name}`;
//       priceParagraph.textContent = `price: ${price}`;
  
//       li.append(authorParagraph, nameParagraph, priceParagraph);
  
//       booksList.append(li);
//     } catch (error) {
//       console.log(error);
//     }
//   });
  
//   root.append(booksList);
  



// Відповідь інструктора
// Оцінка: 70
// Коментарі: 
// код працює, але для advanced js це рішення не годиться. Я кидав файл з рекомендаціями по виконанню ДЗ у слак.  

// на адвансед джинс "просто працює" - не годиться. Звертайтеся до того файлу перед відправкою ДЗ. 

// 1. валідація і рендер прописано в загальному потоці кода. Це потрібно розділити за логікою та загорнути в окремі функції.

// 2. перерахування властивостей вручну, як при рендері так і при валідації полів. А якщо полів буде 50 чи 100, яке буде рішення?

// 3. перевикористання коду. Як можна перевикористати код, якщо немає функцій і все записано в загальному потоці кода. 
// А якщо потрібно перевірити інший масив книг, то заново писати код? А якщо потрібно перевірити на наявність інших полів?


const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];


const root = document.getElementById("root"); // отримуємо елемент з id="root"
const ul = document.createElement("ul"); // створюємо новий елемент ul

// проходимо циклом по масиву books
books.forEach((book) => {
  // перевіряємо, чи містяться всі три властивості
  if (!book.author || !book.name || !book.price) {
    // якщо властивість відсутня, виводимо помилку у консолі
    console.error(`Not full parametrs: ${JSON.stringify(book)}`);
    return; // переходимо до наступного об'єкта
  }

  // якщо всі властивості присутні, створюємо новий елемент li з вмістом книги
  const li = document.createElement("li");
  li.textContent = `${book.author}: ${book.name}, ${book.price}`;
  ul.appendChild(li); // додаємо li до ul
});

root.appendChild(ul); // додаємо ul до елементу з id="root"HW2 - try-catch